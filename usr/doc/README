    --------------------------------------------------------------
         This is the main directory of the debian-wiki project.
    --------------------------------------------------------------

It is coordinated on the debian-www mailing list, debian-www@lists.debian.org
(Go to https://www.debian.org/MailingLists/subscribe#debian-www to subscribe).

How to get started?
===================



How to setup a test site?
=========================

  Let's setup a quick test system, similar to wiki.d.o

  Install pre-requisites
  ----------------------
     # Note: Install the same version as wiki.d.o (i.e Debian Stable)
     # check https://wiki.debian.org/SystemInfo

     aptitude install python-moinmoin \
                      libapache2-mod-wsgi \
                      libapache2-mod-macro \
                      git-core \
                      libjson-perl \
                      libsoap-lite-perl \
                      python-launchpadlib

  System account
  --------------
     On wiki.d.o, the server runs under the account wikiweb (uid=1520)
     which belongs to the group wikiadm (gid=1225).
     You might want to use www-data user and group instead.

     addgroup --gid 1225 wikiadm

     adduser --home /srv/wiki.debian.org/home-unpriv --disabled-password \
         --disabled-login --uid 1520 \
         --gecos "wiki unpriv user" wikiweb

     adduser --home /srv/wiki.debian.org/ --disabled-password \
         --disabled-login --uid 2965 --ingroup wikiadm \
         --gecos "Wiki server account" wiki

     Then add yourself to the list of members of wikiadm:

     addgroup john wikiadm

  Checkout
  ---------
  
     cd ~
     git clone https://salsa.debian.org/debian/wiki.debian.org.git
     # Committers should also set the push URL
     git config url.git@salsa.debian.org:.pushInsteadOf https://salsa.debian.org/

     We recommend to move the repository to /srv. (From now on, this
     documentation assume you moved it)

  Configure the wiki
  ------------------

     sudo mv wiki.debian.org /srv/
     
     Note: The WSGI file used by apache (/srv/wiki.debian.org/bin/moin.wsgi)
           defines the location of moinmoin configuration files as:
             /srv/wiki.debian.org/etc/moin/
           Therefore, the files in /etc/moin/ are not used.
           Also, note wiki.debian.org does not use FarmConfig.


  Create a local branch
  ---------------------

     cd /srv/wiki.debian.org
     git-checkout -b my-local-branch

  Customize your repository
  -------------------------

     cp ./usr/doc/examples/localsecrets.py.template ./etc/moin/localsecrets.py

  Initialise the data
  -------------------

     Let's initialise the wiki's data, like any moinmoin instance.
     (read /usr/share/doc/python-moinmoin/README.Debian.gz)

     mkdir -p /srv/wiki.debian.org/var/moin
     cp -r /usr/share/moin/data /usr/share/moin/underlay /srv/wiki.debian.org/var/moin
     sudo chmod -R g+rwX /srv/wiki.debian.org/var/moin/data
     sudo find /srv/wiki.debian.org/var/moin/data -type d | xargs chmod g+s
     sudo chown -R wiki:wikiweb /srv/wiki.debian.org/var/moin/data
     sudo chown -R wiki:wikiadm /srv/wiki.debian.org/var/moin/data/plugin
     sudo chown -R wiki:wikiweb /srv/wiki.debian.org/var/moin/underlay

     mkdir -p /srv/wiki.debian.org/var/launchpad
     sudo chown -R wiki:wikiweb /srv/wiki.debian.org/var/launchpad

  Configure Apache
  ----------------

     Edit ./etc/apache2/apache2.conf :

       * VirtualHost: Adjust the virtual host (hint: Use an IP like 127.0.12.34
         if your computer is the webserver)
       * Simply comment-out the SSL virtual host.
       * WSGIDaemonProcess : Adjust "user=wikiweb group=wikiweb" if needed.

     ln -s /srv/wiki.debian.org/etc/apache2/apache2.conf /etc/apache2/sites-available/wiki-debian-org
     a2enmod macro wsgi mem_cache
     a2ensite wiki-debian-org
     /etc/init.d/apache2 reload


