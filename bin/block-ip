#!/usr/bin/perl -w
#
# Block specified address(es) from wiki access

use strict;
use Getopt::Long;

my $base = "/srv/wiki.debian.org";
my $etc = "$base/etc/moin";
my $wsgi = "$base/bin/moin.wsgi";
my $deny_file = "$etc/hosts_deny";
my $deny_auto_file = "$etc/hosts_deny_auto";
my $networks_deny_file = "$etc/networks_deny";
my $reason = "";
my $help = '';
my %found;

sub get_time_short()
{
    my @tm;
    my $text;

    @tm = gmtime();
    $text = sprintf("%4d-%02d-%02d",
                    (1900 + $tm[5]),(1 + $tm[4]),$tm[3]);
    return $text;
}

GetOptions('reason=s' => \$reason,
           'help' => \$help);

if ($help) {
    print "block-ip [--reason REASON] ip [ip ip ...]\n";
    print "  Add an ip address to the list of blocked IPs, with a timestamp\n";
    print "  and a reason REASON. If no reason is supplied, \"manual\" will be used.\n";
    exit 0;
}

if ($reason eq "") {
    $reason = "manual";
}

if (scalar(@ARGV) == 0) {
    die "Need an IP address to work with!\n";
}

foreach my $ip (@ARGV) {
    # Trivial input filtering...
    if ($ip =~ /\d+\.\d+\.\d+\.\d+/) {
	# trivial match for IPv4
	$found{$ip} = 0;
    } elsif ($ip =~ /:[[:xdigit:]]+/) {
	# trivial match for IPv6
	$found{$ip} = 0;
    } else {
	print "IGNORE: $ip does not look like a valid IP address to me!\n";
    }
}

foreach my $file ($deny_file, $deny_auto_file, $networks_deny_file) {
    open (IN, "< $file") or print "Unable to open file $file: $!\n";
    while (my $line = <IN>) {
	chomp $line;
	$line =~ s/\s*#.*$//;
	if (length($line) > 2) {
	    if ($file eq $networks_deny_file) {
		foreach my $ip (keys %found) {
		    if ((length($line)) >= 2 && ($ip =~ /^\Q$line\E/)) {
			print "address $ip already found in $file (matches $line), removing from list\n";
			delete $found{$ip};
		    }
		}
	    } else {
		foreach my $ip (keys %found) {
		    if ((length($line)) >= 2 && ($ip =~ /^\Q$line\E$/)) {
			print "address $ip already found in $file, removing from list\n";
			delete $found{$ip};
		    }
		}
	    }
	}
	if (scalar(keys %found) == 0) {
	    print "all addresses already listed, exiting\n";
	    exit;
	}
    }
    close IN;
}

# If we're here, we haven't found the address in our existing list. We
# should add it!
my $timestamp = get_time_short();
open (OUT, ">> $deny_file") or die "Can't open file $deny_file for writing: $!\n";
foreach my $ip (keys %found) {
    print OUT "$ip # $timestamp($reason)\n";
}
close OUT;

# And restart the wiki engine to pick up the change
utime(undef, undef, "$wsgi");

